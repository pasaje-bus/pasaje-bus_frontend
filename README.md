# Pasaje bus Frontend

Esta aplicación frontend está desarrollada con Vue.js

## Requerimientos del sistema

- Gitlab [https://about.gitlab.com/install/](https://about.gitlab.com/install/)
- Nodejs y npm [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)
- Axios [https://www.npmjs.com/package/axios](https://www.npmjs.com/package/axios)
- Bootstrap [https://getbootstrap.com/](https://getbootstrap.com/)
- Vue simple alert [https://vuejsexamples.com/simple-alert-for-vue-js/](https://vuejsexamples.com/simple-alert-for-vue-js/)

## Clonar repositorio

````
git clone https://gitlab.com/pasaje-bus/pasaje-bus_frontend.git
````

## Instalar dependencias del proyecto

````
cd pasaje-bus_frontend/frontend/
npm install
````

## Correr aplicación

````
npm run serve
````

## Consideraciones

- No se consideró la implementación para el login


