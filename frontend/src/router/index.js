import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/nuevaVenta',
    name: 'nuevaVenta',
    component: () => import(/* webpackChunkName: "nuevaVenta" */ '../views/Boleto/NuevaVenta.vue')
  },
  {
    path: '/nuevoChofer',
    name: 'nuevoChofer',
    component: () => import(/* webpackChunkName: "nuevoChofer" */ '../views/Chofer/NuevoChofer.vue')
  },
  // bus
  {
    path: '/agregarBus',
    name: 'agregarBus',
    component: () => import(/* webpackChunkName: "nuevoBus" */ '../views/Bus/NuevoBus.vue')
  },
  {
    path: '/configBus',
    name: 'configBus',
    component: () => import(/* webpackChunkName: "configBus" */ '../views/Bus/ConfigBus.vue')
  },
  {
    path: '/nuevoTrayecto',
    name: 'nuevoTrayecto',
    component: () => import(/* webpackChunkName: "nuevoTrayecto" */ '../views/Trayecto/NuevoTrayecto.vue')
  },
  {
    path: '/nuevoPasajero',
    name: 'nuevoPasajero',
    component: () => import(/* webpackChunkName: "nuevoPasajero" */ '../views/Pasajero/NuevoPasajero.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
